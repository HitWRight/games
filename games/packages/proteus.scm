;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages proteus)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (games packages mono)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (games humble-bundle))

(define-public proteus
  (let* ((version "05162014")
         (file-name (string-append "proteus-" version "-bin"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "Proteus.bin.x86_64")
                   ("i686-linux" "Proteus.bin.x86")
                   (_ "")))
         (other-binary (match (or (%current-target-system)
                                  (%current-system))
                         ("x86_64-linux" "Proteus.bin.x86")
                         ("i686-linux" "Proteus.bin.x86_64")
                         (_ "")))
         (libarch (match (or (%current-target-system)
                             (%current-system))
                    ("x86_64-linux" "64")
                    ("i686-linux" "")
                    (_ ""))))
    (package
      (name "proteus")
      ;; https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=proteus-hib claims
      ;; it's 1.2.
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0hs3nnh557lcvb1a9760g3isyx369y5w9vsnmjcjcbjnx4p57g46"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:validate-runpath? #f    ; TODO: libmono-2.0.so.1 does not validate.
         #:patchelf-plan
         `((,,(string-append "data/" binary)
            ("gcc" "out"))
           ;; TODO: Guix' mono does not work because Proteus checks the
           ;; VERSION.
           (,,(string-append "data/lib" libarch "/libmono-2.0.so.1")
            ("gcc")))
         #:install-plan
         ;; TODO: Replace Mono DLLs with Guix ones, e.g. sdl2-cs.
         `(("data/" "share/proteus/"
            #:exclude (,,other-binary "Proteus")
            #:exclude-regexp ("lib/lib.*" "lib64"))
           (,,(string-append "data/lib" libarch "/libmono-2.0.so.1") "lib/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               ;; We use system* to ignore errors.
               (system* (which "unzip")
                        (assoc-ref inputs "source"))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (wrapper (string-append out "/bin/proteus"))
                      (real (string-append out "/share/proteus/" ,binary))
                      (icon (string-append out "/share/proteus/Proteus.png")))
                 (make-wrapper wrapper real
                               `("LD_LIBRARY_PATH" ":" prefix
                                 ,(map (lambda (lib)
                                         (string-append (assoc-ref inputs lib) "/lib"))
                                       '("sdl2" "sdl2-image" "sdl2-mixer"
                                         "libvorbis" "libogg" "glu"))))
                 (make-desktop-entry-file (string-append out "/share/applications/proteus.desktop")
                                          #:name "Proteus"
                                          #:exec wrapper
                                          #:icon icon
                                          #:comment "Audio-visual exploration game"
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("gcc" ,gcc "lib")
         ("glu" ,glu)
         ("sdl2" ,sdl2)
         ("sdl2-image" ,sdl2-image)
         ("sdl2-mixer" ,sdl2-mixer)
         ("libogg" ,libogg)
         ("libvorbis" ,libvorbis)))
      (home-page "https://www.pcgamingwiki.com/wiki/Proteus") ; REVIEW: http://twistedtreegames.com/proteus/ is offline?
      (synopsis "Audio-visual exploration game")
      (description "Proteus is an exploration and walking simulator video
game.  In the game, the player traverses a procedurally generated environment
without prescribed goals.  The world's flora and fauna emit unique musical
signatures, combinations of which cause dynamic shifts in audio based on the
player's surroundings.")
      (license (undistributable "No URL")))))
