(define-module (games packages terraria)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (nonguix licenses)
  #:use-module (games gog-download)
  #:use-module (games build-system mojo)
  #:use-module (games packages mono))

(define-public gog-terraria
  (let ((buildno "60321"))
    (package
      (name "gog-terraria")
      (version "1.4.4.9_v4")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://terraria/en3installer0")
         (file-name (string-append "terraria_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32
           "0xp8m8c6j809vhcs0q2msyy132mn8lcdjf8m49hy3p6biajnmxly"))))
      (arguments `(#:patchelf-plan
                   (map (lambda (binary)
                          (list binary '("sdl2")))
                        '("Terraria.bin.x86_64"))
                   #:phases
                   (modify-phases %standard-phases
                     (add-after 'install 'wrap-game
                       (lambda* (#:key inputs outputs #:allow-other-keys)
                         (display "lib64") (newline)
                         (let* ((out (assoc-ref outputs "out"))
                                (package (strip-store-file-name out))
                                (prefix (string-append out "/share/" package))
                                (wrapper (string-append prefix "/start.sh"))
                                (ld-lib-path
                                 (append
                                  (map (lambda (lib)
                                         (string-append (assoc-ref inputs lib) "/lib"))
                                       '("sdl2" "mono" "sdl2-image" "libvorbis"))
                                  (list (string-append prefix "/game/lib64")))))
                           (wrap-program wrapper
                             `("LD_LIBRARY_PATH" ":" prefix ,ld-lib-path))))))))
      (inputs
       `(("sdl2" ,sdl2)
         ("sdl2-image" ,sdl2-image)
         ("mono" ,mono)
         ("libvorbis" ,libvorbis)))
      (build-system mojo-build-system)
      (supported-systems '("x86_64-linux"))
      (home-page "https://www.terraria.org/")
      (synopsis "action-adventure sandbox game developed by Re-Logic")
      (description "Terraria is a 2D sandbox game with gameplay that revolves
around exploration, building, crafting, combat, survival, and mining, playable
in both single-player and multiplayer modes.
The game has a 2D sprite tile-based graphical style reminiscent of the 16-bit
sprites found on the Super NES. The game is noted for its classic
exploration-adventure style of gameplay, similar to games such as the Metroid
series and Minecraft.")
      (license (undistributable "No URL")))))
